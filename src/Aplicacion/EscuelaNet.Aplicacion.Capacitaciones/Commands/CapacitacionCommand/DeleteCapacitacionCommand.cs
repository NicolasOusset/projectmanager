﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionCommand
{
    public class DeleteCapacitacionCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }


        public int IDLugar { get; set; }
        public Lugar Lugar { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
        public int Duracion { get; set; }
        public decimal Precio { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }

        public EstadoDeCapacitacion Estado { get; set; }
    }
}

﻿using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.Commands
{
    public class UpdateLugarCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public int Capacidad { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Depto { get; set; }
        public string Piso { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Pais { get; set; }
    }
}

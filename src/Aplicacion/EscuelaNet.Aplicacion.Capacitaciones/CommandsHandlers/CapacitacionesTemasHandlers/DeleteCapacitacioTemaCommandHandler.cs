﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionTemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.CapacitacionesTemasHandlers
{
    public class DeleteCapacitacioTemaCommandHandler : IRequestHandler<DeleteCapacitacionTemaCommand, CommandRespond>
    {
        private ICapacitacionRepository _capacitacionRepository;
        private ITemaRepository _temaRepository;

        public DeleteCapacitacioTemaCommandHandler(ICapacitacionRepository capacitacionRepository, ITemaRepository temaRepository)
        {
            _capacitacionRepository = capacitacionRepository;
            _temaRepository = temaRepository;
        }

        public Task<CommandRespond> Handle(DeleteCapacitacionTemaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

           
                var capacitacionBuscada = _capacitacionRepository.GetCapacitacion(request.IDCapacitacion);
                var temabuscado = _temaRepository.GetTema(request.IDTema);
                
                    
                    capacitacionBuscada.pullTema(temabuscado);

                _capacitacionRepository.Update(capacitacionBuscada);
                _capacitacionRepository.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

        }
    }
}

﻿using Dapper;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices.CapacitacionServices
{
    public class CapacitacionesQuery : ICapacitacionesQuery
    {
        private string _connectionString;

        public CapacitacionesQuery(string connectionString)
        {
            _connectionString = connectionString;
        }
        public CapacitacionQueryModel GetCapacitacion(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<CapacitacionQueryModel>(
                    @"
                    SELECT t.IDCapacitacion as Id,
                    t.IDLugar as IDLugar,
                    t.Minimo as Minimo,
                    t.Maximo as Maximo,
                    t.Precio as Precio,
                    t.Inicio as Inicio,
                    t.Duracion as Duracion,
                    t.Estado as Estado,
                    t.Fin as Fin
                    FROM Capacitaciones as t
                    WHERE IDCapacitacion = @id
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<CapacitacionQueryModel> ListCapacitaciones()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<CapacitacionQueryModel>(
                    @"
                    SELECT t.IDCapacitacion as Id,
                    t.IDLugar as IDLugar,
                    t.Minimo as Minimo,
                    t.Maximo as Maximo,
                    t.Precio as Precio,
                    t.Inicio as Inicio,
                    t.Duracion as Duracion,
                    t.Estado as Estado,
                    t.Fin as Fin,
                    l.Calle as Calle, l.Numero as Numero, l.Depto as Depto, l.Piso as Piso
                    FROM Capacitaciones as t
                    INNER JOIN Lugares as l ON t.IDLugar = l.IDLugar
                    WHERE l.IDLugar = t.IDLugar
                   ").ToList();
            }
        }
    }

}

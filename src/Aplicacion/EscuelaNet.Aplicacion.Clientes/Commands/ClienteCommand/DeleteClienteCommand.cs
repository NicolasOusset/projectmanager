﻿using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;

namespace EscuelaNet.Aplicacion.Clientes.Commands.ClienteCommand
{
    public class DeleteClienteCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string RazonSocial { get; set; }
        public string Email { get; set; }
        public Categoria Categoria { get; set; }

    }
}

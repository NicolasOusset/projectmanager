﻿using EscuelaNet.Aplicacion.Clientes.Commands.ClienteCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.ClienteCommandHandler
{
    class DeleteClienteCommandHandler :
        IRequestHandler<DeleteClienteCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public DeleteClienteCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(DeleteClienteCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var cliente = _clienteRepositorio.GetCliente(request.Id);
                _clienteRepositorio.Delete(cliente);
                _clienteRepositorio.UnitOfWork.SaveChanges();
                
                responde.Succes = true;
                return Task.FromResult(responde);
            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}

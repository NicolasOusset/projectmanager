﻿using EscuelaNet.Aplicacion.Clientes.Commands.DireccionCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.DireccionCommandHandler
{
    public class NuevaDireccionCommandHandler :
        IRequestHandler<NuevaDireccionCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public NuevaDireccionCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(NuevaDireccionCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Domicilio))
            {
                try
                {
                    var domicilio = request.Domicilio;
                    var localidad = request.Localidad;
                    var provincia = request.Provincia;
                    var pais = request.Pais;

                    var unidadDeNegocio = _clienteRepositorio.GetUnidadDeNegocio(request.IdUnidad);
                    var direccion = new Direccion(domicilio, localidad, provincia, pais);

                    unidadDeNegocio.AgregarDireccion(direccion);
                    _clienteRepositorio.Update(unidadDeNegocio.Cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();
                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos.";
                return Task.FromResult(responde);
            }
        }
    }
}

﻿using EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.SolicitudCommandHandler
{
    public class NuevaSolicitudCommandHandler :
        IRequestHandler<NuevaSolicitudCommand, CommandRespond>
    {
        private ISolicitudRepository _solicitudesRepositorio;

        public NuevaSolicitudCommandHandler(ISolicitudRepository solicitudesRepositorio)
        {
            _solicitudesRepositorio = solicitudesRepositorio;
        }


        public Task<CommandRespond> Handle(NuevaSolicitudCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Titulo))
            {
                try
                {
                    var solicitud = new Solicitud(request.Titulo, request.Descripcion);
                    _solicitudesRepositorio.Add(solicitud);
                    _solicitudesRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);

                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos.";
                return Task.FromResult(responde);
            }
        }
    }
}

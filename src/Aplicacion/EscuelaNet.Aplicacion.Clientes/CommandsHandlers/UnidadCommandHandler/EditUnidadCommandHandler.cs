﻿using EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.UnidadCommandHandler
{
    public class EditUnidadCommandHandler :
        IRequestHandler<EditUnidadCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public EditUnidadCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(EditUnidadCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.RazonSocial)
                 || !string.IsNullOrEmpty(request.ResponsableDeUnidad)
                 || !string.IsNullOrEmpty(request.Cuit)
                 || !string.IsNullOrEmpty(request.EmailResponsable)
                 || !string.IsNullOrEmpty(request.TelefonoResponsable))
            {
                try
                {
                    var cliente = _clienteRepositorio.GetCliente(request.IdCliente);
                    cliente.Unidades.Where(un => un.ID == request.IdUnidad).First().RazonSocial = request.RazonSocial;
                    cliente.Unidades.Where(un => un.ID == request.IdUnidad).First().ResponsableDeUnidad = request.ResponsableDeUnidad;
                    cliente.Unidades.Where(un => un.ID == request.IdUnidad).First().Cuit = request.Cuit;
                    cliente.Unidades.Where(un => un.ID == request.IdUnidad).First().EmailResponsable = request.EmailResponsable;
                    cliente.Unidades.Where(un => un.ID == request.IdUnidad).First().TelefonoResponsable = request.TelefonoResponsable;

                    _clienteRepositorio.Update(cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos.";
                return Task.FromResult(responde);
            }
        }
    }
}

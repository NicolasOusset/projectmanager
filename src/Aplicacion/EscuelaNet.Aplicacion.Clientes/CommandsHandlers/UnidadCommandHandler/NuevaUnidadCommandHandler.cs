﻿using EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.UnidadCommandHandler
{
    class NuevaUnidadCommandHandler :
        IRequestHandler<NuevaUnidadCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public NuevaUnidadCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(NuevaUnidadCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.RazonSocial)
                || !string.IsNullOrEmpty(request.ResponsableDeUnidad)
                || !string.IsNullOrEmpty(request.Cuit)
                || !string.IsNullOrEmpty(request.EmailResponsable)
                || !string.IsNullOrEmpty(request.TelefonoResponsable))
            {
                try
                {
                    var razonSocial = request.RazonSocial;
                    var responsable = request.ResponsableDeUnidad;
                    var cuit = request.Cuit;
                    var email = request.EmailResponsable;
                    var telefono = request.TelefonoResponsable;

                    var cliente = _clienteRepositorio.GetCliente(request.IdCliente);

                    var unidad = new UnidadDeNegocio(razonSocial, responsable, cuit, email, telefono);
                    cliente.AgregarUnidad(unidad);
                    _clienteRepositorio.Update(cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);

                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos.";
                return Task.FromResult(responde);
            }
        }
    }
}

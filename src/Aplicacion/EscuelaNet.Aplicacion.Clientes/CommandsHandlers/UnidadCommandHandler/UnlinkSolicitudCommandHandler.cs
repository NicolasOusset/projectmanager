﻿using EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.UnidadCommandHandler
{
    class UnlinkSolicitudCommandHandler :
        IRequestHandler<UnlinkSolicitudCommand, CommandRespond>
    {
        private IClienteRepository _clientesRepositorio;

        public UnlinkSolicitudCommandHandler(IClienteRepository clientesRepositorio)
        {
            _clientesRepositorio = clientesRepositorio;
        }

        public Task<CommandRespond> Handle(UnlinkSolicitudCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(request.IDUnidad);
                var solicitudBuscada = unidadBuscada.Solicitudes.First(s => s.ID == request.IDSolicitud);

                unidadBuscada.PullSolicitud(solicitudBuscada);

                _clientesRepositorio.Update(unidadBuscada.Cliente);
                _clientesRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}

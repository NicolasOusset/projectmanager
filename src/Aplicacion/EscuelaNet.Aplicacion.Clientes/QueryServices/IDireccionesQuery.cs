﻿using EscuelaNet.Aplicacion.Clientes.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public interface IDireccionesQuery
    {
        DireccionesQueryModel GetDireccion(int id);

        List<DireccionesQueryModel> ListDirecciones(int id);
    }
}

﻿using EscuelaNet.Aplicacion.Conocimiento.Responds;
using MediatR;

namespace EscuelaNet.Aplicacion.Conocimiento.Commands.CategoriaCommand
{
    public class DeleteCategoriaCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}

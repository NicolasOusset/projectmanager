﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class NuevoProgramadorCommandHandler : IRequestHandler<NuevoProgramadorCommand, CommandRespond>
    {
        private IProgramadorRepository _programadorRepositorio;
        public NuevoProgramadorCommandHandler(IProgramadorRepository programadorRepositorio)
        {
            _programadorRepositorio = programadorRepositorio;
        }
        public Task<CommandRespond> Handle(NuevoProgramadorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    _programadorRepositorio.Add(new Programador(request.Nombre, request.Apellido, request.Legajo, request.Dni,request.Rol,request.FechaNacimiento));
                    _programadorRepositorio.UnitOfWork.SaveChanges();
                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "El nombre está vacio.";
                return Task.FromResult(responde);
            }
        }
    }
}

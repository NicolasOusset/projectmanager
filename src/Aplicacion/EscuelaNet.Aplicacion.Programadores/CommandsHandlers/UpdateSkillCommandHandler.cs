﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class UpdateSkillCommandHandler : IRequestHandler<UpdateSkillCommand, CommandRespond>
    {
        private ISkillRepository _skillRepositorio;
        public UpdateSkillCommandHandler(ISkillRepository skillRepositorio)
        {
            _skillRepositorio = skillRepositorio;
        }
        public Task<CommandRespond> Handle(UpdateSkillCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                try
                {
                    var skill = _skillRepositorio.GetSkill(request.Id);
                    skill.Descripcion = request.Descripcion;
                    skill.Grados = request.Grados;

                    _skillRepositorio.Update(skill);
                    _skillRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Texto vacio";
                return Task.FromResult(responde);
            }

        }
    }
}

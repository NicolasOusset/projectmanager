﻿using EscuelaNet.Aplicacion.Programadores.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.QueryServices
{
    public interface IProgramadorQuery
    {
        ProgramadorQueryModel GetProgramador(int id);
        List<ProgramadorQueryModel> ListProgramador();
    }
}

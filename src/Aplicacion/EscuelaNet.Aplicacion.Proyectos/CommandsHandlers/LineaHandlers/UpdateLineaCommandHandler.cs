﻿using EscuelaNet.Aplicacion.Proyectos.Commands.LineaCommand;
using EscuelaNet.Aplicacion.Proyectos.Responds;
using EscuelaNet.Dominio.Proyectos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.CommandsHandlers.LineaHandlers
{
    public class UpdateLineaCommandHandler : IRequestHandler<UpdateLineaCommand, CommandRespond>
    {
        private ILineaRepository _lineaRepository;
        public UpdateLineaCommandHandler(ILineaRepository lineaRepository)
        {
            _lineaRepository = lineaRepository;
        }
        public Task<CommandRespond> Handle(UpdateLineaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (string.IsNullOrEmpty(request.Nombre))
            {
                responde.Success = false;
                responde.Error = "Nombre vacío";
                return Task.FromResult(responde);
            }
            else
            {
                try
                {
                    var linea = _lineaRepository.GetLinea(request.ID);
                    linea.Nombre = request.Nombre;
                    _lineaRepository.Update(linea);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    responde.Success = true;
                    return Task.FromResult(responde);
                }catch(Exception ex)
                {
                    responde.Success = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
        }
    }
}

﻿using Dapper;
using EscuelaNet.Aplicacion.Proyectos.QueryModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.QueryServices.LineaServices
{
    public class LineaQuery : ILineaQuery
    {
        public string _connectionString;

        public LineaQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public LineaQueryModel GetLinea(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<LineaQueryModel>(
                    @"SELECT l.IDLinea as ID,
                    l.Nombre as Nombre,
                    FROM LineasDeProduccion as l
                    WHERE IDLinea = @id
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }
        public List<LineaQueryModel> ListLinea()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<LineaQueryModel>(
                    @"SELECT l.IDLinea as ID,
                    l.Nombre as Nombre
                    FROM LineasDeProduccion as l").ToList();
            }
        }
    }
}

﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class Asesor : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public Disponibilidad Disponibilidad { get; set; }
        public string Idioma { get; set; }
        public String Pais { get; set; }
        public IList<Conocimiento>Conocimientos { get; set; }
        
        public Asesor(string nombre, string apellido, string idioma, String pais)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre),nameof(apellido));
            this.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(apellido));
            this.Idioma = idioma;
            this.Pais = Pais;
        }
        private Asesor()
        {

        }

        public void pullConocimiento(Conocimiento conocimiento)
        {
            if (this.Conocimientos != null)
            {
                if (this.Conocimientos.Contains(conocimiento))
                {
                    this.Conocimientos.Remove(conocimiento);
                }
            }
        }

        public void pushConocimiento(Conocimiento conocimiento)
        {
            if (this.Conocimientos == null)
            {
                this.Conocimientos = new List<Conocimiento>();
            }
            if(!this.Conocimientos.Contains(conocimiento))
            {
                this.Conocimientos.Add(conocimiento);
            }
            else {
                throw new Exception("Ya está el conocimiento vinculado al Asesor");
            }
        }
       
    }
}

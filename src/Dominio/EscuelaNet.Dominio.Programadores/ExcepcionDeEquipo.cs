﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class ExcepcionDeEquipo : Exception
    {
        public ExcepcionDeEquipo(string message) : base(message)
        { }
    }
}

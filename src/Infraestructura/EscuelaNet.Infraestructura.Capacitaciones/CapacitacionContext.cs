﻿using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Hierarchy;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Capacitaciones
{
    public class CapacitacionContext : DbContext, IUnitOfWork
    {
        public DbSet<Tema> Temas { get; set; }
        public DbSet<Instructor> Instructores { get; set; }
        public DbSet<Lugar> Lugares { get; set; }
        public DbSet<Dominio.Capacitaciones.Capacitacion> Capacitaciones { get; set; }
        public CapacitacionContext() : base("Context")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TemasEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new InstructoresEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new LugaresEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new CapacitacionesEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}

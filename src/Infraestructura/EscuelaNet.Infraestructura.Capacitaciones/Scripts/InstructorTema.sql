﻿USE [AdministradorDeProyectos1]
GO

/****** Object:  Table [dbo].[InstructorTema]    Script Date: 11/9/2019 11:02:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InstructorTema](
	[IDInstructor] [int] NOT NULL,
	[IDTema] [int] NOT NULL,
 CONSTRAINT [PK_InstructorTema] PRIMARY KEY CLUSTERED 
(
	[IDInstructor] ASC,
	[IDTema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos.Repositorios
{
    public class ConocimientoRepositorio : IConocimientoRepository

    {
        private CategoriaContext _contexo = new CategoriaContext();
        public IUnitOfWork UnitOfWork => _contexo;

        public Conocimiento Add(Conocimiento conocimiento)
        {
            _contexo.Conocimientos.Add(conocimiento);
            return conocimiento;
        }

        public void Update(Conocimiento conocimiento)
        {
            _contexo.Entry(conocimiento).State = EntityState.Modified;
        }

        public void Delete(Conocimiento conocimiento)
        {
            _contexo.Conocimientos.Remove(conocimiento);
        }

        public Conocimiento GetConocimiento(int id)
        {
            var conocimiento = _contexo.Conocimientos.Find(id);
            if (conocimiento != null)
            {
                _contexo.Entry(conocimiento);
            }
            return conocimiento;
        }

        public List<Conocimiento> ListConocimiento()
        {
            return _contexo.Conocimientos.ToList();
        }
    }
}

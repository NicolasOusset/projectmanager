﻿USE [AdministradorDeProyectos]
GO

/****** Object:  Table [dbo].[Asesor]    Script Date: 15/9/2019 20:36:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Asesor](
	[IDAsesor] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](20) NOT NULL,
	[Apellido] [varchar](20) NOT NULL,
	[Disponibilidad] [int] NOT NULL,
	[Idioma] [varchar](10) NOT NULL,
	[Pais] [varchar](20) NOT NULL,
	[conocimiento_ID] [int] NOT NULL,
 CONSTRAINT [PK_Asesor] PRIMARY KEY CLUSTERED 
(
	[IDAsesor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
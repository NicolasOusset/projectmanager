USE [AdministradorDeProyectos1]
GO

/****** Object:  Table [dbo].[UnidadSolicitud]    Script Date: 12/09/2019 16:19:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UnidadSolicitud](
	[IDUnidadDeNegocio] [int] NOT NULL,
	[IDSolicitud] [int] NOT NULL,
 CONSTRAINT [PK_UnidadSolicitud] PRIMARY KEY CLUSTERED 
(
	[IDUnidadDeNegocio] ASC,
	[IDSolicitud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



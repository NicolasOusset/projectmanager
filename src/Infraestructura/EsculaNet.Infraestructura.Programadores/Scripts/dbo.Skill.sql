USE [AdministradorDeProyectos]
GO

/****** Object:  Table [dbo].[Skill]    Script Date: 15/9/2019 22:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Skill](
	[IdSkill] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](75) NOT NULL,
	[Grados] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdSkill] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



﻿using Autofac;
using Autofac.Integration.Mvc;
using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura.AutofacModules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Capacitacion.Web.App_Start
{
    public static class IoCConfigurator
    {
        public static void ConfigurarIoC()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule(new MediatRModule());
            builder.RegisterModule(new ApplicationModule());
            var container = builder.Build();
            DependencyResolver.SetResolver(
                new AutofacDependencyResolver(
                container));
        }
    }
}
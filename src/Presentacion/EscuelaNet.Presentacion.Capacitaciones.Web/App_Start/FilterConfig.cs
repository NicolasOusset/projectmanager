﻿using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Capacitaciones.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

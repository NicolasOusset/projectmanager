﻿using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitaciones.Web.Models
{
    public class NewInstructorModel : Instructor
    {
        public int Id { get; set; }
    }
}
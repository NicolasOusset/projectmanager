﻿//using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura;
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Conocimientos;
using EscuelaNet.Infraestructura.Conocimientos.Repositorios;


namespace EscuelaNet.Presentacion.Conocimientos.Web.Controllers
{
    public class ConocimientoController : Controller
    {
        private ICategoriaRepository _categoriaRepository;
        private IAsesorRepository _repositorioAsesor;

        public ConocimientoController(ICategoriaRepository categoriaRepository, IAsesorRepository repositorioAsesor)
        {
            _categoriaRepository = categoriaRepository;
            _repositorioAsesor = repositorioAsesor;
        }
        
        public ActionResult Index(int id)
        {
            var categoria = _categoriaRepository.GetCategoria(id);
            if (categoria.Conocimientos == null)
            {
                TempData["error"] = "Categoria sin Conocimientos";
                return RedirectToAction("../Categoria/Index");
            }
            var model = new CategoriaIndexModel()
            {
                Titulo = "Conocimientos de la Categoría: '" + categoria.Nombre + "'",
                Conocimientos = categoria.Conocimientos.ToList(),
                IdCategoria = id
            };
            return View(model);
        }

        public ActionResult New(int id)
        {
            var categoria = _categoriaRepository.GetCategoria(id);
            var model = new NuevaConocimientoModel()
            {
                Titulo = "Nuevo Conocimiento para la Categoria: '" + categoria.Nombre + "'",
                IdCate = id
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaConocimientoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var nombre = model.Nombre;
                    var demanda = model.Demanda;

                    var categoria = _categoriaRepository.GetCategoria(model.IdCate);

                    var conocimiento = new Conocimiento(nombre);
                    categoria.AgregarConocimiento(conocimiento);
                    _categoriaRepository.Update(categoria);
                    _categoriaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "¡Unidad creada!";
                    return RedirectToAction("Index/" + model.IdCate);

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio, complete los campos solicitados";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {

            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var categoria = _categoriaRepository.GetConocimieto(id);

                var model = new NuevaConocimientoModel()
                {
                    Titulo = "Editar el Conocimiento: '" + categoria.Nombre + "' de la Categoría: '" + categoria.Categoria.Nombre + "'",
                    IdCate = categoria.Categoria.ID,
                    IdConocimiento = id,
                    Nombre = categoria.Nombre,
                    Demanda = categoria.Demanda,
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaConocimientoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
                
            {
                try
                {
                    var categoria = _categoriaRepository.GetCategoria(model.IdCate);
                    categoria.Conocimientos.Where(un => un.ID == model.IdConocimiento).First().Nombre = model.Nombre;
                    categoria.Conocimientos.Where(un => un.ID == model.IdConocimiento).First().Demanda = model.Demanda;
                    
                    _categoriaRepository.Update(categoria);
                    _categoriaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "¡Conocimiento editado!";
                    return RedirectToAction("Index/" + model.IdCate);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id < 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var conocimiento = _categoriaRepository.GetConocimieto(id);
                var model = new NuevaConocimientoModel()
                {
                    Titulo = "Borrar el conocimiento: '" + conocimiento.Nombre + "' de la Categoria: '" + conocimiento.Categoria.Nombre + "'",
                    IdCate = conocimiento.Categoria.ID,
                    IdConocimiento = id,
                    Nombre = conocimiento.Nombre,
                    Demanda = conocimiento.Demanda,
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaConocimientoModel model)
        {
            try
            {
                var conocimiento = _categoriaRepository.GetConocimieto(model.IdCate);
                _categoriaRepository.DeleteConocimiento(conocimiento);
                _categoriaRepository.UnitOfWork.SaveChanges();

                TempData["success"] = "¡Conocimiento borrado!";
                return RedirectToAction("../Conocimiento/Index/" + model.IdCate);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult Asesor(int id)
        {
            var conocimiento = _categoriaRepository.GetConocimieto(id);
            var model = new AsesorConocimientoModel()
            {
                Conocimiento = conocimiento,
                Asesores = conocimiento.Asesores.ToList()
            };
            return View(model);
        }
    }
}



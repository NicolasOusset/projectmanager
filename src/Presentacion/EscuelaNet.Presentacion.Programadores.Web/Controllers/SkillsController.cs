﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.QueryServices;
using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using EsculaNet.Infraestructura.Programadores.Repositorios;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class SkillsController : Controller
    {
        private ISkillRepository _repositorio;
        private ISkillQuery _skillQuery;
        private IMediator _mediator;

        public SkillsController(ISkillRepository skillRepository, 
            ISkillQuery skillQuery, IMediator mediator)
        {
            _repositorio = skillRepository;
            _skillQuery = skillQuery;
            _mediator = mediator;
        }
        // GET: Skills
        public ActionResult Index()
        {
            var skill = _skillQuery.ListSkill();
           
            var model = new SkillsIndexModel()
            {
                Titulo = "Nuevos Conocimientos",
                Skills = skill
            };

            return View(model);
        }

        // GET: Skills/Create
        public ActionResult Create()
        {
            var model = new NuevoSkillsModel();
            return View(model);
        }

        // POST: Skills/Create
        [HttpPost]
        public async Task<ActionResult> Create(NuevoSkillCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Conocimiento creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoSkillsModel()
                {
                    Descripcion = model.Descripcion,
                    Grados = model.Grados
                };
                return View(modelReturn);
            }
        }

        // GET: Skills/Edit/5
        public ActionResult Edit(int id)
        {
           var skill = _skillQuery.GetSkill(id);
           var model = new NuevoSkillsModel()
           {
            Descripcion = skill.Descripcion,
            Grados = skill.Grados,
            IdSkills = id
           };

           return View(model);    
        }

        // POST: Skills/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UpdateSkillCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Conocimiento Modificado Correctamente.";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoSkillsModel()
                {
                    Descripcion = model.Descripcion,
                    Grados = model.Grados
                };
                return View(modelReturn);
            }
        }
     
        // GET: Skills/Delete/5
        public ActionResult Delete(int id)
        {
                var skill = _skillQuery.GetSkill(id);
                var model = new NuevoSkillsModel()
                {
                    Descripcion = skill.Descripcion,
                    Grados = skill.Grados,
                    IdSkills = id
                };

                return View(model);      
    
        }

        // POST: Skills/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(DeleteSkillCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Conocimiento Borrado Correctamente.";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoSkillsModel()
                {
                    Descripcion = model.Descripcion,
                    Grados = model.Grados
                };
                return View(modelReturn);
            }
        }
    }
}

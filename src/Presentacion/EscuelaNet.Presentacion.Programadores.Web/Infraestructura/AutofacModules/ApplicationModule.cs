﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Autofac;
using EscuelaNet.Aplicacion.Programadores.QueryServices;
using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Infraestructura.Programadores.Repositorios;
using EsculaNet.Infraestructura.Programadores;
using EsculaNet.Infraestructura.Programadores.Repositorios;

namespace EscuelaNet.Presentacion.Programadores.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {

        protected override void Load(ContainerBuilder builder)
        {
            var entorno = ConfigurationManager.AppSettings["Entorno"];
            switch (entorno)
            {
                case "Produccion":
                    var connectionString = 
                        ConfigurationManager
                            .ConnectionStrings["EquipoContext"].ToString();

                    builder.RegisterType<EquipoContext>()
                        .InstancePerRequest();


                    builder.Register(c => new EquipoQuery(
                            connectionString))
                        .As<IEquipoQuery>()
                        .InstancePerLifetimeScope();

                    builder.Register(c => new ProgramadorQuery(
                            connectionString))
                        .As<IProgramadorQuery>()
                        .InstancePerLifetimeScope();

                    builder.Register(c => new SkillQuery(
                            connectionString))
                        .As<ISkillQuery>()
                        .InstancePerLifetimeScope();

                    builder.RegisterType<EquiposRepository>()
                        .As<IEquipoRepository>()
                        .InstancePerLifetimeScope();

                    builder.RegisterType<SkillsRepository>()
                        .As<ISkillRepository>()
                        .InstancePerLifetimeScope();

                    builder.RegisterType<ProgramadoresRepository>()
                       .As<IProgramadorRepository>()
                       .InstancePerLifetimeScope();

                    break;
                case "Singleton":
                default:
                    builder.RegisterType<EquipoSingletonRepository>()
                        .As<IEquipoRepository>()
                        .InstancePerLifetimeScope();
                    break;

            }

            base.Load(builder);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class NuevaTecnologiaModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
    }
}